##Option 3: Use bind-mounted persistent volumes

The images contain the SonarQube installation at /opt/sonarqube. You can use bind-mounted persistent volumes to override selected files or directories, for example:

    sonarqube_conf:/opt/sonarqube/conf: configuration files, such as sonar.properties
    sonarqube_data:/opt/sonarqube/data: data files, such as the embedded H2 database and Elasticsearch indexes
    sonarqube_logs:/opt/sonarqube/logs
    sonarqube_extensions:/opt/sonarqube/extensions: plugins, such as language analyzers


##If you're running on Linux, you must ensure that:
  
      vm.max_map_count is greater or equals to 262144
      fs.file-max is greater or equals to 65536
      the user running SonarQube can open at least 65536 file descriptors
      the user running SonarQube can open at least 2048 threads
  
  You can see the values with the following commands :
  sysctl vm.max_map_count
  sysctl fs.file-max
  ulimit -n
  ulimit -u
  
  You can set them dynamically for the current session by running  the following commands as root:
  sysctl -w vm.max_map_count=262144
  sysctl -w fs.file-max=65536
  ulimit -n 65536
  ulimit -u 2048

##sharing conf and data files

