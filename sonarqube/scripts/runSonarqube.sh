docker run -d --name sonarqube \
    -p 9000:9000 \
    -v sonarqube_conf:/opt/sonarqube/conf \
    -v sonarqube_data:/opt/sonarqube/data \
    -v sonarqube_logs:/opt/sonarqube/logs \
    -v sonarqube_extensions:/opt/sonarqube/extensions \
    -e sonar.jdbc.username=sonar \
    -e sonar.jdbc.password=sonar \
    sonarqube